<?php

$driver = 'mysql';
const HOST = 'localhost';
const DB_NAME = 'hillel';
const DB_USER = 'root';
const DB_PASSWORD = 'SenS2021';
$charset = 'utf8';
$options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];

try {
    $pdo = new PDO("$driver:HOST; DB_NAME; charset=$charset",DB_USER,DB_PASSWORD,$options);
}catch (PDOException$errorConnect){
    die("Не могу подключиться к базе данных");
}

$result = $pdo->query('SELECT * FROM hillel.clients');

$row = $result->fetch(PDO::FETCH_ASSOC);

while ($row = $result->fetch(PDO::FETCH_ASSOC)){
    echo $row['first_name'] . "\n".
         $row['last_name'] . "\n" . 'email'. "\n" .
         $row['email'] . "\n" . 'companu_name' . "\n".
         $row['company_name'] . "\n" . 'is_active' . "\n" .
         $row['is_active'] . "\n" . 'age ' .
         $row['age'] . '<br>';
};

$row = $result->fetchAll((PDO::FETCH_ASSOC));
echo '<pre>';
var_dump($row) . "\n\n";

$result = $pdo->query('SELECT first_name, last_name, is_active  as active FROM hillel.clients
WHERE is_active LIKE is_active IS NOT NULL');

$row = $result->fetchAll((PDO::FETCH_ASSOC));
echo '<pre>';
var_dump($row);

$result = $pdo->query('SELECT first_name, last_name, age  FROM hillel.clients
WHERE age >= 30');

$row = $result->fetchAll((PDO::FETCH_ASSOC));
echo '<pre>';
var_dump($row);

$result = $pdo->query("SELECT first_name FROM hillel.clients
WHERE first_name LIKE 'в%'");

$row = $result->fetchAll((PDO::FETCH_ASSOC));
echo '<pre>';
var_dump($row);

$result = $pdo->query("SELECT COUNT(first_name) as all_clients
FROM hillel.clients");

$row = $result->fetchAll((PDO::FETCH_ASSOC));
echo '<pre>';
var_dump($row);

$result = $pdo->query("SELECT max(age) as old FROM hillel.clients");

$row = $result->fetchAll((PDO::FETCH_ASSOC));
echo '<pre>';
var_dump($row);

$result = $pdo->query("SELECT COUNT(is_active) as active
FROM hillel.clients
WHERE is_active IS NOT NULL");

$row = $result->fetchAll((PDO::FETCH_ASSOC));
echo '<pre>';
var_dump($row);

$result = $pdo->query("SELECT first_name, last_name, age
FROM hillel.clients
ORDER BY age");

$row = $result->fetchAll((PDO::FETCH_ASSOC));
echo '<pre>';
var_dump($row);

$result = $pdo->query("SELECT first_name, last_name
FROM hillel.clients
ORDER BY first_name");

$row = $result->fetchAll((PDO::FETCH_ASSOC));
echo '<pre>';
var_dump($row);

$result = $pdo->query("SELECT COUNT(is_active) as '>25'
FROM hillel.clients
WHERE is_active IS NOT NULL AND age > 25");

$row = $result->fetchAll((PDO::FETCH_ASSOC));
echo '<pre>';
var_dump($row);

$client_id = 2;
$age = 45;
$data = $pdo->prepare('UPDATE hillel.clients SET age = :age WHERE client_id = :client_id');
$data->execute([':client_id' => $client_id, ':age' => $age]);

$client_id = 1;
$first_name = 'Виктор';
$data = $pdo->prepare('UPDATE hillel.clients SET first_name = :first_name WHERE client_id = :client_id');
$data->execute([':client_id' => $client_id, ':first_name' => $first_name]);

$client_id = 3;
$is_active = 0;
$data = $pdo->prepare('UPDATE hillel.clients SET is_active = :is_active WHERE client_id = :client_id');
$data->execute([':client_id' => $client_id, ':is_active' => $is_active]);

$is_active = 0;
$sql = "DELETE FROM hillel.clients WHERE is_active = :is_active";
$stmt = $pdo->prepare($sql);
$stmt->execute([':is_active' => $is_active]);

$sql = "DELETE FROM hillel.clients";
$stmt = $pdo->prepare($sql);
$stmt->execute();